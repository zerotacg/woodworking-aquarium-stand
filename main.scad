$s = 0.5;

height = 800;
width = 400;
depth = 400;

leg_size = [71,71];
support_size= [34,72];
wheel_base_size = [60,60,2];
wheel_diameter = 50;
wheel_height = 70;
wheel_turn_diameter = 110;
wheel_overhang = 0.5*wheel_turn_diameter + 0.5* wheel_base_size[0] - leg_size[0];
support_overlap = support_size[0] - wheel_overhang;

main();

module main() {
    legs();
    supports();
    wheels();
}

module legs() {
    leg();

    translate([depth, 0, 0])
    rotate([0,0,1*90])
    leg();

    translate([depth, width, 0])
    rotate([0,0,2*90])
    leg();

    translate([0, width, 0])
    rotate([0,0,3*90])
    leg();
}

module leg() {
    cutout([support_overlap, leg_size[1], support_size[1]]) {
        cutout([leg_size[0],support_overlap, support_size[1]]) {
            cube([leg_size[0], leg_size[1], height]);
        }
    }
}

module cutout(size) {
    difference() {
        children();
        
        translate([-$s, -$s, -$s])
        cube([size[0] + 2*$s,size[1] + 2*$s,size[2] + 2*$s]);
    }
}

module supports() {
    supportsFrame();

    translate([0, 0, -support_size[1]])
    translate([0, 0, height])
    supportsFrame();
}

module supportsFrame() {
    supportWidth();

    translate([depth, width, 0])
    rotate([0,0,180])
    supportWidth();

    supportDepth();

    translate([depth, width, 0])
    rotate([0,0,180])
    supportDepth();
}

module supportWidth() {
    supportPart(width);
}

module supportDepth() {
    translate([depth,0,0])
    rotate([0,0,90])
    supportPart(depth);
}

module supportPart(length) {
    translate([0,-$s,0])
    translate([-support_size[0],0,0])
    translate([support_overlap,0,0])
    cube([support_size[0], length, support_size[1]]);
}

module wheels() {
    translate([0,0,-$s]) {
        translate(-[wheel_base_size[0],wheel_base_size[1],0])
        translate([leg_size[0],leg_size[1],0])
        wheel();
    }
}

module wheel() {
    translate([0,0,-wheel_base_size[2]])
    cube(wheel_base_size);
    
    translate([0.5*wheel_base_size[0],0.5*wheel_base_size[1],0])
    translate(0.5*[0,wheel_diameter,wheel_diameter])
    translate(-[0,0.5*wheel_turn_diameter,wheel_height])
    rotate([0,90,0])
    cylinder(h=10,d=wheel_diameter, center=true);
}